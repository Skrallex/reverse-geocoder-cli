# General CPack settings
set(CPACK_PACKAGE_CONTACT "Skrallex")
set(CPACK_PACKAGE_HOMEPAGE_URL "https://gitlab.com/Skrallex/reverse-geocoder-cli")

# Package long description
set(DEBIAN_PACKAGE_LONG_DESCRIPTION
" This packages provides a Linux command-line interface for converting a set of\n\
 coordinates into a geocode location. By using the reverse-geocoder library, an\n\
 internet connection is not required.\n\
 .\n\
 Depending on the location, typical output includes: country code, country name,\n\
 administration level 1 (eg. state), administration level 2 (eg. city) and the\n\
 timezone (no daylight-savings compensation). The executable provides a number of\n\
 different optional arguments to alter the output format.")

# Settings for .deb packaging
if(CPACK_GENERATOR MATCHES "DEB")
	set(CPACK_DEBIAN_PACKAGE_DEPENDS "libc6 (>= 2.2.5), libgcc1 (>= 1:3.0), libstdc++6 (>= 4.8), reverse-geocoder-lib (>= 0.1.2)")
	set(CPACK_DEBIAN_PACKAGE_SECTION "utils")
	set(CPACK_DEBIAN_PACKAGE_PRIORITY "optional")
	set(CPACK_DEBIAN_PACKAGE_HOMEPAGE "https://gitlab.com/Skrallex/reverse-geocoder-cli")
	set(CPACK_DEBIAN_PACKAGE_DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION_SUMMARY}\n${DEBIAN_PACKAGE_LONG_DESCRIPTION}")
	set(CPACK_DEBIAN_FILE_NAME "DEB-DEFAULT")
endif()

if(CPACK_GENERATOR MATCHES "TGZ")
endif()