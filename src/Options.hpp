#pragma once

#include <fstream>
#include <map>
#include <string>

namespace options
{
    const static std::map<std::string, double> distance_unit_map =
    {
        // Typical units
        { "mm", 1000.0           },
        { "cm", 100.0            },
        { "m",  1.0              },
        { "km", 1.0/1000.0       },
        { "in", 39.3701          },
        { "ft", 3.28084          },
        { "yd", 1.09361          },
        { "mi", 1.0/1609.34      },

        // Novelty units
        { "au",     1.0/149597870700.0 },
        { "earths", 1.0/6372800.0      },
        { "moons",  1.0/1737100.0      },
    };

    // ================================ VARIABLES =============================== //

    // Input variables
    extern std::string executable_name;
    extern double latitude;
    extern double longitude;
    extern std::string distance_unit;

    // Output variables
    extern int print_distance;            // If the distance from the location should be print (default true)
    extern int print_timezone;            // If the timezone should be printed (default true)
    extern std::string output_file_path;  // The file path that the output will be written to (default none)
}

// ================================= METHODS ================================ //

/**
 * @brief  Analyses the command-line arguments for valid options/flags
 * @param  argc: Argument count integer
 * @param  argv: Argument c-string vector
 */
void processArguments(int argc, char** argv);

/**
 * @brief  Prints out the concise program usage.
 */
void printUsage();

/**
 * @brief  Prints out the detailed program usage.
 */
void printFullUsage();