#include "Options.hpp"

#include <iostream>
#include <reverse-geocoder/rgeocoder.hpp>

int main(int argc, char** argv)
{
	// Process the command-line arguments first
	processArguments(argc, argv);

	// Find the closest point
    rgeocoder::Geocode closest_geocode = rgeocoder::getNearestGeocode(options::latitude, options::longitude);

	// Print the distance to the Geocode if enabled
	if(options::print_distance)
	{
		double distance_m = rgeocoder::distanceBetweenMetres(options::latitude, options::longitude, closest_geocode.latitude, closest_geocode.longitude);

		// Print in metres
		double distance_converted = options::distance_unit_map.at(options::distance_unit) * distance_m;
		std::cout.imbue(std::locale(""));
		std::cout << std::fixed << distance_converted << " " << options::distance_unit << " away from ";
	}

	// Print the locality name
	if(!closest_geocode.name.empty())
		std::cout << closest_geocode.name;

	// Print the administration level 2 name
	if(!closest_geocode.admin_2.empty())
		std::cout <<  ", " << closest_geocode.admin_2;

	// Print the administration level 1 name
	if(!closest_geocode.admin_1.empty())
		std::cout << ", " << closest_geocode.admin_1;

	// Print the country code, converted to the country name
	if(!closest_geocode.country_code.empty())
		std::cout << ", " << rgeocoder::countryCodeToName(closest_geocode.country_code);

	// Print the timezone if enabled
	if(!closest_geocode.timezone.empty() && options::print_timezone)
		std::cout << " (TZ: " << closest_geocode.timezone << ")";

    std::cout << std::endl;
}
