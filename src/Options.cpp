#include "Options.hpp"

#include <iostream>
#include <reverse-geocoder/rgeocoder.hpp>
#include <vector>

#include <error.h>
#include <getopt.h>

// Make-file #def
#ifndef AUTO_FILL_VERSION
    #define AUTO_FILL_VERSION ""
#endif

namespace options
{
    // ================================ VARIABLES =============================== //

    // Input variables
    std::string executable_name;
    double latitude = 0;
    double longitude = 0;
    std::string distance_unit = "km";

    // Output variables
    int print_distance = 1;
    int print_timezone = 1;
    std::string output_file_path =  "";
}

// ================================= METHODS ================================ //

void processArguments(int argc, char** argv)
{
    // Set the executable name from the first argument
    options::executable_name = std::string(argv[0]);

    // Construct the short options string
    static const std::string short_opts =
        "h"
        "o:"
        "u:"
        "v";

    // Construct the long options in the required struct
    static struct option long_opts[] =
    {
        // These options don't directly set a variable
        { "help",    no_argument,       nullptr, 'h' },
        { "output",  required_argument, nullptr, 'o' },
        { "units",   required_argument, nullptr, 'u' },
        { "version", no_argument,       nullptr, 'v' },

        // These options directly set a variable
        { "no-print-distance", no_argument, &options::print_distance, 0 },
        { "no-print-timezone", no_argument, &options::print_timezone, 0 },

        // Terminating element is required
        { 0, 0, 0, 0 }
    };

    // Make sure there are some arguments
    if(argc < 2)
    {
        printUsage();
        exit(0);
    }

    // Check for negative coordinates
    std::vector<std::string> saved_args;
    for(int i = 1; i < argc; i++)
    {
        // Check if the arg starts with '-'
        if(argv[i][0] == '-')
        {
            // Check if it's a negative number, ie. -7
            if(argv[i][1] >= '0' && argv[i][1] <= '9')
            {
                saved_args.push_back(argv[i]);
                argv[i][0] = 0;
            }
        }
        else
        {
            saved_args.push_back(argv[i]);
        }
    }

    // Loop until all the options are exhausted
    while(true)
    {
        int option_index = 0;

        // Get the next option
        int c = getopt_long(argc, argv, short_opts.c_str(), long_opts, &option_index);

        // Detect if there are no more options
        if(c == -1)
        {
            break;
        }

        // Switch-case for all the option characters
        switch(c)
        {
            // Returns 0 only on long options that directly set a flag, so we don't need to do anything
            case 0:
                break;

            // Help
            case 'h':
                printFullUsage();
                exit(0);
                break;

            // Output file
            case 'o':
            {
                options::output_file_path = optarg;

                // Check if we can open the provided file path
                std::ofstream file;
                file.open(options::output_file_path.c_str(), std::fstream::out);

                // If opening the file failed, error and exit
                if(file.fail())
                {
                    error_at_line(1, errno, __FILE__, __LINE__, "Couldn't open output file '%s'", options::output_file_path.c_str());
                }
                break;
            }

            // Units
            case 'u':
            {
                std::string entered_units = optarg;
                if(options::distance_unit_map.count(entered_units) < 1)
                {
                    error_at_line(1, 0, __FILE__, __LINE__, "Invalid units entered: %s", entered_units.c_str());
                }
                else
                {
                    options::distance_unit = entered_units;
                }

                break;
            }

            // Version
            case 'v':
                std::cout << "reverse-geocoder version " << AUTO_FILL_VERSION << std::endl;
                std::cout << "librevgeo version " << rgeocoder::VERSION << std::endl;
                exit(0);
                break;

            // Unknown option
            case '?':
                // Getopt already prints an error message, no reason to print another
                break;

            default:
                error_at_line(1, 0, __FILE__, __LINE__, "Unrecognised option caused error");
                break;
        }
    }

    // Process the saved non-option arguments
    if(saved_args.size() < 2)
    {
        printUsage();
        exit(1);
    }

    options::latitude = std::stod(saved_args[0]);
    options::longitude = std::stod(saved_args[1]);
}

void printUsage()
{
    static const std::string usage =
        "reverse-geocoder version " + std::string(AUTO_FILL_VERSION) + "\n"
        "\n"
        "Usage: " + options::executable_name + " [options] latitude longitude\n"
        "Try '-h' or '--help' for full usage details\n"
        ;

    std::cout << usage << std::endl;
}

void printFullUsage()
{
    // Construct the usage message
    static const std::string full_usage =
        "reverse-geocoder version " + std::string(AUTO_FILL_VERSION) + "\n"
        "\n"
        "Takes in latitude and longitude and finds the nearest locality name through\n"
        "the use of librgeocoder.\n"
        "Released under the GNU GPLv3 license\n"
        "\n"
        "Usage: " + options::executable_name + " [options] latitude longitude\n"
        "\n"
        "Mandatory arguments:\n"
        "     latitude                 the latitude in decimal degrees\n"
        "     longitude                the longitude in decimal degrees\n"
        "\n"
        "Optional arguments:\n"
        " -h, --help                   shows this usage information\n"
        " -o, --output <file>          file path to write the output to\n"
        " -u, --units <units>          unit to use for distance, default is km\n"
        "                              see 'Valid units' section below\n"
        " -v, --version                prints the version details and exits\n"
        "\n"
        "     --no-print-distance      disables printing the distance from the locality\n"
        "     --no-print-timezone      disables printing the timezone\n"
        "\n"
        "Valid units:\n"
        " Metric\n"
        "   mm                         millimetres\n"
        "   cm                         centimetres\n"
        "   m                          metres\n"
        "   km                         kilometres (default)\n"
        "\n"
        " Imperial\n"
        "   in                         inches\n"
        "   ft                         feet\n"
        "   yd                         yards\n"
        "   mi                         miles\n"
        "\n"
        " Other\n"
        "   au                         astronomical units - distance from Earth to sun\n"
        "   earths                     radius of the Earth\n"
        "   moons                      radius of the moon\n"
        ;
    ;

    std::cout << full_usage << std::endl;
}
